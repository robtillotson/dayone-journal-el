# Day One for Emacs

Day One is a very nice journal app for iOS and the Mac, but while it
stores its data in a reasonably portable form, there is currently no
way to work with it on other platforms short of hand-editing XML
files.  And really, who wants to do THAT?

This project will eventually be a usable mode to work with a Day One
journal in Emacs.  The intent is for it to be able to create new
entries and edit existing ones, with support for entry text and tags
but probably not locations, weather, and pictures (except perhaps for
viewing this information if it is added from the iOS/Mac app).

Currently all there is is a parser for the Day One entry files, and
a very bare skeleton for an interactive mode that currently does
nothing...
